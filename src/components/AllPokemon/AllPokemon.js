import React, { Component } from 'react';
import { Button, Segment, Input } from 'semantic-ui-react'
import { PokeList } from '../../components';
import PropTypes from 'prop-types';

export default class AllPokemon extends Component {
  static propTypes = {
    getAllPokemon: PropTypes.func,
    loading: PropTypes.bool,
    pokemon: PropTypes.array,
    addPokemon: PropTypes.func,
    removePokemon: PropTypes.func,
    myPokemon: PropTypes.array,
    push: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      limit: 20,
      search: ''
    }
  }

  componentDidMount() {
    this.props.getAllPokemon()
  }

  loadMoreItems = () => {
    const { page } = this.state;
    this.props.getAllPokemon(page + 1);
    this.setState({ page: page + 1 })
  }

  isInitialLoad = () => this.props.loading && this.state.page === 1

  onSearchChange = (e, { value }) => this.setState({ search: value })

  render() {
    return (
      <Segment loading={this.isInitialLoad()}>
        <Input
          placeholder='Find Pokemon by ID/Name'
          value={this.state.search}
          onChange={this.onSearchChange}
          action={{ icon: 'search', onClick: () => this.state.search ? this.props.push(`/pokemon/${this.state.search}`) : null }}
        />
        <PokeList
          pokemon={this.props.pokemon}
          addPokemon={this.props.addPokemon}
          removePokemon={this.props.removePokemon}
          myPokemon={this.props.myPokemon}
          push={this.props.push}
        />
        {this.props.pokemon.length < this.props.count ? <Button centered loading={this.props.loading} onClick={this.loadMoreItems}>Load More</Button> : null}
      </Segment>
    );
  }
}
