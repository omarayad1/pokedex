import React, { Component } from 'react';
import { Menu, Container } from 'semantic-ui-react';
import { PropTypes } from 'prop-types';

export default class MainLayout extends Component {
  static propTypes = {
    pathname: PropTypes.string,
    push: PropTypes.func,
    children: PropTypes.object
  }

  render() {
    return (
      <Container style={{ paddingTop: '10px', paddingBottom: '10px'}}>
        <Menu inverted size='tiny'>
          <Menu.Item
            name='pokemon'
            active={this.props.pathname === '/pokemon'}
            onClick={() => this.props.push('/pokemon')}
          >
            All Pokemon
          </Menu.Item>
          <Menu.Item
            name='myPokemon'
            active={this.props.pathname === '/mypokemon'}
            onClick={() => this.props.push('/mypokemon')}
          >
            My Pokemon
          </Menu.Item>
        </Menu>
        {this.props.children}
      </Container>
    )
  }
}
