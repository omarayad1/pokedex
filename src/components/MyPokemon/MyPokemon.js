import React, { Component } from 'react';
import { PokeList } from '../../components';
import PropTypes from 'prop-types';

export default class MyPokemon extends Component {
  static propTypes = {
    myPokemon: PropTypes.array,
    addPokemon: PropTypes.func,
    removePokemon: PropTypes.func,
    push: PropTypes.func
  }

  render() {
    return (
      <PokeList
        pokemon={this.props.myPokemon}
        addPokemon={this.props.addPokemon}
        removePokemon={this.props.removePokemon}
        myPokemon={this.props.myPokemon}
        push={this.props.push}
      />
    );
  }
}
