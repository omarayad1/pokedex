import React, { Component } from 'react';
import { Image, Card, Label, Segment } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const getBadgeDetail = (title, detail) => <Label color="blue" size="mini">
  {title}
  <Label.Detail>{detail}</Label.Detail>
</Label>

export default class PokeDetails extends Component {
  static propTypes = {
    match: PropTypes.object,
    getPokemon: PropTypes.func,
    pokemon: PropTypes.object,
    loading: PropTypes.bool,
    error: PropTypes.object
  }

  componentDidMount() {
    const { match: { params }, getPokemon} = this.props;
    getPokemon(params.id)
  }

  render() {
    const { pokemon } = this.props;
    if (!this.props.loading && this.props.error) {
      return (
        <div>No Pokemon Found</div>
      )
    }
    return (
      <Segment loading={this.props.loading}>
        <Card centered fluid>
          <Image.Group size='tiny'>
            {pokemon ? Object.values(pokemon.sprites).filter(sprite => sprite).map(sprite => <Image src={sprite} />) : null}
          </Image.Group>
          <Card.Content>
            <Card.Header>{pokemon ? pokemon.name : null}</Card.Header>
            <Card.Meta>#{pokemon ? pokemon.id.toString().padStart(3, '0') : null}</Card.Meta>
            {pokemon ? <Card.Description>
              {getBadgeDetail('Base Exp.', pokemon.base_experience)}
              {getBadgeDetail('Height', pokemon.height)}
              {getBadgeDetail('Weight', pokemon.weight)}
            </Card.Description> : null}
          </Card.Content>
          <Card.Content extra>
            <h3>Types</h3>
            {pokemon ? pokemon.types.map(type => <Label key={type.type.id} size="mini">{type.type.name}</Label>) : null}
          </Card.Content>
          <Card.Content extra>
            <h3>Moves</h3>
            {pokemon ? pokemon.moves.map(move => <Label key={move.move.id} size="mini">{move.move.name}</Label>) : null}
          </Card.Content>
        </Card>
      </Segment>
    );
  }
}
