import React, { Component } from 'react';
import { List, Image, Button, Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const getPokeIdFromUrl = url => url.split('/')[6]

export default class PokeList extends Component {
  static propTypes = {
    pokemon: PropTypes.array,
    myPokemon: PropTypes.array,
    addPokemon: PropTypes.func,
    removePokemon: PropTypes.func,
    push: PropTypes.func
  }

  render() {
    return (
      <List divided verticalAlign='middle' centered>
        {this.props.pokemon.map(poke => (
          <List.Item>
            <List.Content floated='right'>
              <Button.Group size='tiny'>
                {!this.props.myPokemon.find(somePoke => somePoke.name === poke.name)
                  ? <Button basic color='green' onClick={() => this.props.addPokemon(poke)}>
                      <Icon name="add"/>
                    </Button>
                  : <Button basic color='red' onClick={() => this.props.removePokemon(poke.name)}>
                    <Icon name="remove"/>
                  </Button>}
                <Button basic color='blue' onClick={() => this.props.push(`/pokemon/${getPokeIdFromUrl(poke.url)}`)}><Icon name="external alternate"/></Button>
              </Button.Group>
            </List.Content>
            <Image avatar src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${getPokeIdFromUrl(poke.url)}.png`}/>
            <List.Content>{poke.name} – #{getPokeIdFromUrl(poke.url).padStart(3, '0')}</List.Content>
          </List.Item>
        ))}
      </List>
    );
  }
}
