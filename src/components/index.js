export { default as MainLayout } from './MainLayout/MainLayout';
export { default as AllPokemon } from './AllPokemon/AllPokemon';
export { default as PokeDetails } from './PokeDetails/PokeDetails';
export { default as MyPokemon } from './MyPokemon/MyPokemon';
export { default as PokeList } from './PokeList/PokeList';
