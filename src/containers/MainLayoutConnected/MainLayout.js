import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { MainLayout } from '../../components';
import { withRouter } from 'react-router';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  pathname: state.router.location.pathname
});

const mapDispatchToProps = { push };

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainLayout))
