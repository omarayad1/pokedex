import { addPokemon, removePokemon } from '../../redux/actions/myPokemon';
import { connect } from 'react-redux';
import { MyPokemon } from '../../components';
import { push } from 'connected-react-router';

const mapStateToProps = state => ({
  myPokemon: state.myPokemon.myPokemon
})

const mapDispatchToProps = { addPokemon, removePokemon, push }

export default connect(mapStateToProps, mapDispatchToProps)(MyPokemon)
