import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { PokeDetails } from '../../components';
import { getPokemon } from '../../redux/actions/pokemon';

const mapStateToProps = state => ({
  pokemon: state.pokemon.poke,
  loading: state.pokemon.loading,
  error: state.pokemon.error
})

const mapDispatchToProps = { getPokemon };

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PokeDetails))
