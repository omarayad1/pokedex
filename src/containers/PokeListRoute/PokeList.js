import { getAllPokemon } from '../../redux/actions/pokemon';
import { addPokemon, removePokemon } from '../../redux/actions/myPokemon';
import { connect } from 'react-redux';
import { AllPokemon } from '../../components';
import { push } from 'connected-react-router';

const mapStateToProps = state => ({
  pokemon: state.pokemon.allPoke,
  myPokemon: state.myPokemon.myPokemon,
  count: state.pokemon.count,
  loading: state.pokemon.loading
})

const mapDispatchToProps = { getAllPokemon, addPokemon, removePokemon, push }

export default connect(mapStateToProps, mapDispatchToProps)(AllPokemon)
