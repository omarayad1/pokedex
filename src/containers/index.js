export { default as PokeListRoute } from './PokeListRoute/PokeList';
export { default as MyPokemon } from './MyPokeRoute/PokeList';
export { default as PokeDetails } from './PokeDetailsRoute/PokeDetails';
export { default as MainLayout } from './MainLayoutConnected/MainLayout';
