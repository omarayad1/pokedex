import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import store, { history, persistor } from './redux/store';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import routes from './routes';
import 'semantic-ui-css/semantic.min.css';
import { PersistGate } from 'redux-persist/integration/react'

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <div>
          <Switch>
            {routes.map(route => {
              const { layout: Layout, component: Component, ...otherProps } = route;
              if (Layout) {
                return <Route component={() => <Layout><Component /></Layout>} {...otherProps} />
              }
              return <Route component={() => <Component />} {...otherProps} />
            })}
          </Switch>
        </div>
      </ConnectedRouter>
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
