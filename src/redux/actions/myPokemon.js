import {
  ADD_POKEMON,
  REMOVE_POKEMON
} from '../constants/myPokemon';

export const addPokemon = data => dispatch => {
  dispatch({
    type: ADD_POKEMON,
    data
  })
}

export const removePokemon = name => dispatch => {
  dispatch({
    type: REMOVE_POKEMON,
    name
  })
}
