import pokeClient from '../../helpers/pokeClient';
import {
  GET_ALL_POKEMON,
  GET_ALL_POKEMON_SUCCESS,
  GET_ALL_POKEMON_FAIL,

  GET_POKEMON,
  GET_POKEMON_SUCCESS,
  GET_POKEMON_FAIL
} from '../constants/pokemon';

export const getAllPokemon = (page = 1, limit = 20) => dispatch => {
  dispatch({
    type: GET_ALL_POKEMON,
    page
  });
  pokeClient
    .get('pokemon', { params: { offset: (page - 1) * limit, limit }})
    .then(({data}) => dispatch({
      type: GET_ALL_POKEMON_SUCCESS,
      data
    }))
    .catch(error => dispatch({
      type: GET_ALL_POKEMON_FAIL,
      error
    }))
}

export const getPokemon = pokeId => dispatch => {
  dispatch({
    type: GET_POKEMON
  });
  pokeClient
    .get(`pokemon/${pokeId}`)
    .then(({data}) => dispatch({
      type: GET_POKEMON_SUCCESS,
      data
    }))
    .catch(error => dispatch({
      type: GET_POKEMON_FAIL,
      error
    }))
}
