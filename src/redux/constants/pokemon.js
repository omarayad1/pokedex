export const GET_ALL_POKEMON = 'pokemon/GET_ALL_POKEMON';
export const GET_ALL_POKEMON_SUCCESS = 'pokemon/GET_ALL_POKEMON_SUCCESS';
export const GET_ALL_POKEMON_FAIL = 'pokemon/GET_ALL_POKEMON_FAIL';

export const GET_POKEMON = 'pokemon/GET_POKEMON';
export const GET_POKEMON_SUCCESS = 'pokemon/GET_POKEMON_SUCCESS';
export const GET_POKEMON_FAIL = 'pokemon/GET_POKEMON_FAIL';
