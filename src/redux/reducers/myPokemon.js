import {
  ADD_POKEMON,
  REMOVE_POKEMON
} from '../constants/myPokemon';

const initialState = {
  myPokemon: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_POKEMON:
      return {
        ...state,
        myPokemon: [...state.myPokemon, action.data]
      }
    case REMOVE_POKEMON:
      return {
        ...state,
        myPokemon: state.myPokemon.filter(pokemon => pokemon.name !== action.name)
      }
    default:
      return state
  }
}
