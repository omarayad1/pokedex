import {
  GET_ALL_POKEMON,
  GET_ALL_POKEMON_SUCCESS,
  GET_ALL_POKEMON_FAIL,

  GET_POKEMON,
  GET_POKEMON_SUCCESS,
  GET_POKEMON_FAIL
} from '../constants/pokemon';

const initialState = {
  allPoke: [],
  poke: null,
  loading: false,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_POKEMON:
    case GET_POKEMON:
      return {
        ...state,
        poke: null,
        loading: true,
        allPoke: action.page && action.page === 1 ? [] : state.allPoke
      }
    case GET_ALL_POKEMON_SUCCESS:
      return {
        ...state,
        loading: false,
        allPoke: [...state.allPoke, ...action.data.results],
        count: action.data.count
      }
    case GET_POKEMON_SUCCESS:
      return {
        ...state,
        loading: false,
        poke: action.data
      }
    case GET_ALL_POKEMON_FAIL:
    case GET_POKEMON_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error
      }
    default:
      return state
  }
}
