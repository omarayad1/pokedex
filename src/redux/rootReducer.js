import { combineReducers } from 'redux';
import pokemon from './reducers/pokemon';
import myPokemon from './reducers/myPokemon';

export default combineReducers({
  pokemon,
  myPokemon
});
