import {
  PokeListRoute,
  MyPokemon,
  PokeDetails,
  MainLayout
} from './containers';

export default [
  {
    path: '/',
    component: PokeListRoute,
    exact: true,
    layout: MainLayout
  }, {
    path: '/pokemon',
    component: PokeListRoute,
    exact: true,
    layout: MainLayout
  }, {
    path: '/pokemon/:id',
    component: PokeDetails,
    exact: true,
    layout: MainLayout
  }, {
    path: '/mypokemon',
    component: MyPokemon,
    exact: true,
    layout: MainLayout
  }
]
